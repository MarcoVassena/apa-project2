APA Project 2


Authors: Marco Vassena,    4110161
         Philipp Hausmann, 4003373



===========
BUILDING
===========

There are two solutions, the unify approach one inside the folder "unify" and
the constraints based one inside the folder "constraints". Execute the following statements
inside one of these two folders.

Prerequisites (plain cabal build dependencies are omitted; other versions might work too):
 * GHC 7.4.2
 * uuagc 0.9.50
 * uuagc-cabal 1.0.5.0
 * cco 0.0.4 (http://www.cs.uu.nl/wiki/Cco/CourseResources#CCO_Library)

Building:
$ cabal configure --enable-tests
$ cabal build


Running the programs:
$ ./dist/build/main/main <FILE>


Running the tests:
$ cabal test

==========
Examples
==========

See the "examples/" directories inside the "unify"/"constraints" folder.

The examples for both approaches are identical,
but the recorded expected solutions inside the "examples/tests/" folder
may differ. All expected solutions are sound, but some of the solutions
for the unify-approach are less optimal than for the constraint-based approach.

==========
DOCUMENTATION
==========

See doc/Doc.pdf for the complete documentation.
See also the haddock documentation (dist/doc/).

