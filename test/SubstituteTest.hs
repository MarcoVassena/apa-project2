-- | This module contains lexer tests for test-driven-development

module Main where

import Test.HUnit
import CCO.Converter.TypeInference
import CCO.SystemF.Base (Ty(..), TyVar)
import System.Exit (exitFailure)


-- | The single tests that will be run
tests :: Test
tests = TestList [testSubAsoc, testAppl, testInst]

testSubAsoc :: Test
testSubAsoc = tySub ~~ (TyVar "c") ~?= (TyVar "a")
  where tySub = ((TyVar "b") :-> (TyVar "a")) . ((TyVar "c") :-> (TyVar "b"))

testAppl :: Test
testAppl = TestCase $ do
  s ~~ (TyVar "a") @?= (TyVar "b")
  s ~~ (TyVar "b") @?= (TyVar "b")
  s ~~ (TyVar "c") @?= (TyVar "c")

  s ~~ (Arr (TyVar "a") (TyVar "a")) @?= (Arr (TyVar "b") (TyVar "b"))
  s ~~ (Arr (TyVar "b") (TyVar "b")) @?= (Arr (TyVar "b") (TyVar "b"))
  s ~~ (Arr (TyVar "c") (TyVar "c")) @?= (Arr (TyVar "c") (TyVar "c"))


  s ~~ (Forall "a" (TyVar "a")) @?= (Forall "a" (TyVar "a"))
  s ~~ (Forall "b" (TyVar "a")) @?= (Forall "b" (TyVar "b"))
  s ~~ (Forall "c" (TyVar "c")) @?= (Forall "c" (TyVar "c"))

  where s = (TyVar "a") :-> (TyVar "b")

testInst :: Test
testInst = TestCase $ do
  inst [TyVar "b", TyVar "c"] (Forall "a" (TyVar "a")) @?= ([TyVar "c"], TyVar "b")
  inst [TyVar "c", TyVar "d", TyVar "e"] (Forall "a" (Forall "b" (Arr (TyVar "a") (TyVar "b")))) @?= ([TyVar "e"], Arr (TyVar "c") (TyVar "d"))

main :: IO ()
main = do
  successH <- runTestTT tests >>= return . passH
  if successH then return () else exitFailure

-- | Returns whether some 'HUnit' test failed
passH :: Counts -> Bool
passH result = failures result == 0 && errors result == 0

