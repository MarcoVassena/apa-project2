-- | This module contains lexer tests for test-driven-development

module Main where

import Test.HUnit
import CCO.SourcePos
import System.Exit (exitFailure)
import System.IO
import System.Directory
import qualified Data.Map as M
import CCO.HM (parseFile)
import CCO.HM.AlphaEq
import Data.List (isSuffixOf)

testDir = "examples/tests"


-- TODO import this from the lib as soon as implemented
transform :: a -> a
transform = undefined

buildTests :: String -> IO [Test]
buildTests dir = do
  files <- getDirectoryContents dir
  let inps = getFiles files ".inp.lam"
  let exps = getFiles files ".exp.lam"
  let tests = M.intersectionWith (,) inps exps

  return $ map makeTest (M.toList tests)


  where getFiles fs suf = M.fromList [ (take (length f - length suf) f, f) | f <- fs, isSuffixOf suf f]
        makeTest (k, (inp, exp)) = TestLabel k $ TestCase $ do
            hmInp <- parseFile (testDir ++ "/" ++ inp)
            hmExp <- parseFile (testDir ++ "/" ++ exp)

            let hmTrans = transform hmInp

            let msg = "expected: " ++ show hmExp ++ "\n but got: " ++ show hmTrans

            (hmExp `alphaEq` hmTrans) @? msg
  
  
main :: IO ()
main = do

  tests <- buildTests testDir

  successH <- runTestTT (TestList tests) >>= return . passH
  if successH then return () else exitFailure

-- | Returns whether some 'HUnit' test failed
passH :: Counts -> Bool
passH result = failures result == 0 && errors result == 0

