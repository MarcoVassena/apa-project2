module CCO.PPrint.AnnType where

import CCO.PPrint.Base
import CCO.Analysis.AnnType

instance PPrint AnnTy where
  pprint (ATyVar v) = v
  pprint ((:-->) psi1 t1 phi t2 psi2) = unwords [pprint t1, psi1, "-[ " ++ phi ++ " ]->", pprint t2, psi2]
  pprint ATyUnit = "()"

instance PPrint ATy where
  pprint (ATy ty phi psi) = pprint ty ++ " (" ++ phi ++ "," ++ psi ++ ")"
