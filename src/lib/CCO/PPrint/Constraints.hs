module CCO.PPrint.Constraints where

import CCO.PPrint.Base
import CCO.Constraints

instance (Show a) => PPrint (Constraint a) where
  pprint (Constraint _ lhs op rhs) = unwords [pprint lhs, pprint op, pprint rhs]

instance (Show a) => PPrint (CTerm a) where
  pprint (CFinal f) = pprint f
  pprint (CJoin x y) = unwords [pprint x, "bjoin", pprint y]
  pprint (CMeet x y) = unwords [pprint x, "bmeet", pprint y]
  
instance (Show a) => PPrint (CFinal a) where
  pprint (CVar v) = v
  pprint (CVal v) = show v

instance PPrint COp where
  pprint UnderOrEqual   = "<:="
  pprint Equal          = "=:="
