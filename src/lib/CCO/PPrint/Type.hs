module CCO.PPrint.Type where

import CCO.PPrint.Base
import CCO.Analysis.Type

instance PPrint Ty where
  pprint (TyVar ty) = ty
  pprint (a :-> b) = pprint a ++ " -> " ++ pprint b
  pprint TyUnit = "()"
 
