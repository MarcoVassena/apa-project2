module CCO.PPrint.Relevance where

import CCO.Analysis.Relevance
import CCO.PPrint.Base

instance PPrint Relevance where
  pprint S = "S"
  pprint L = "L"
