{-# LANGUAGE FlexibleInstances #-}

module CCO.PPrint.Base where

class PPrint a where
  pprint :: a -> String

instance PPrint [Char] where
  pprint s = s

instance (PPrint a, PPrint b) => PPrint (a, b) where
  pprint (x ,y) = "(" ++ pprint x ++ ", " ++ pprint y ++ ")"
