module CCO.PPrint.Lang where

import CCO.PPrint.Base
import CCO.HM.Base

instance PPrint Tm where
  pprint (Tm _ t) = pprint t

instance PPrint Tm_ where
  pprint (Var x) = x
  pprint (Lam x t) = "(\\ " ++ x ++ " . " ++ pprint t ++ ")"
  pprint (App t1 t2) = unwords $ map pprint [t1, t2]
  pprint (StrictApp t1 t2) = unwords [pprint t1, "$!", pprint t2]
  pprint Unit = "()"
  pprint Bottom = "~"
