{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}

module CCO.Constraints.RelBuilder
  (-- var
--  , val
    IsCFinal (..)
  , IsCTerm (..)
  , bjoin
  , bmeet
  , (<:=)
  , (=:=)
  , Relevance (..)
  )
where

import CCO.SourcePos
import CCO.Analysis.Lattice
import CCO.Constraints.Base
import CCO.Analysis.Relevance

class Lattice b => IsCFinal a b where
  toCFinal :: a -> CFinal b

instance IsCFinal Relevance Relevance where
  toCFinal v = CVal v

instance IsCFinal String Relevance where
  toCFinal s = CVar s

{-val :: Lattice a => a -> CFinal a
val = CVal

var :: Lattice a => String -> CFinal a
var = CVar-}

class Lattice b => IsCTerm a b where
  toCTerm :: a -> CTerm b

instance IsCTerm String Relevance where
  toCTerm = CFinal . CVar

instance IsCTerm Relevance Relevance where
  toCTerm = CFinal . CVal

instance IsCTerm (CTerm Relevance) Relevance where
  toCTerm t = t


bjoin :: (IsCFinal a Relevance, IsCFinal b Relevance)=> a -> b -> CTerm Relevance
bjoin x y = CJoin (toCFinal x) (toCFinal y)

bmeet :: (IsCFinal a Relevance, IsCFinal b Relevance)=> a -> b -> CTerm Relevance
bmeet x y = CMeet (toCFinal x) (toCFinal y)

(<:=) :: (IsCTerm b Relevance, IsCTerm c Relevance) => b -> c -> SourcePos -> Constraint Relevance
(<:=) e1 e2 s = Constraint s (toCTerm e1) UnderOrEqual (toCTerm e2)

(=:=) :: (IsCTerm b Relevance, IsCTerm c Relevance) => b -> c -> SourcePos -> Constraint Relevance
(=:=) e1 e2 s = Constraint s (toCTerm e1) Equal (toCTerm e2)

(>:=) :: (IsCTerm b Relevance, IsCTerm c Relevance) => b -> c -> SourcePos -> Constraint Relevance
(>:=) e1 e2 s = undefined
