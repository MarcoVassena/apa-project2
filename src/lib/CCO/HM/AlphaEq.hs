{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}

module CCO.HM.AlphaEq
  ( AlphaEq (..)
  )

where

import CCO.HM.Base
import Data.Map
import Control.Monad.State

type Subst = Var -> Var

class AlphaEq a where
  alphaEq :: a -> a -> Bool

class AlphaEq' a where
  alphaEq' :: Subst -> Subst -> a -> a -> EqM Bool

-- fresh variables
type EqM = State [Var]

instance (AlphaEq' a) => AlphaEq a where
  -- We just assume that these identifiers are not being used by the user.
  alphaEq t1 t2 = evalState (alphaEq' id id t1 t2) ["internal__" ++ show i ++ "+" | i <- [0..]]

freshVar :: EqM Var
freshVar = do
  (v:vs) <- get
  put vs
  return v

instance AlphaEq' Tm where
  alphaEq' s1 s2 (Tm _ t1) (Tm _ t2) = alphaEq' s1 s2 t1 t2

instance AlphaEq' Tm_ where
  alphaEq' s1 s2 (Var x) (Var y) = return $ (s1 x) == (s2 y)
  alphaEq' s1 s2 (Lam x1 t1) (Lam x2 t2)     = do
        x' <- freshVar
        -- rename x2 and x1 to identical fresh variable. This avoids accidental captures.
        alphaEq' (s1 . (x1 --> x')) (s2 . (x2 --> x')) t1 t2
  alphaEq' s1 s2 (App t1a t1b) (App t2a t2b) = appEq s1 s2 t1a t1b t2a t2b
  alphaEq' s1 s2 (StrictApp t1a t1b) (StrictApp t2a t2b) = appEq s1 s2 t1a t1b t2a t2b
  alphaEq' s1 s2 (Unit)   (Unit) = return True
  alphaEq' s1 s2 (Bottom) (Bottom) = return True
  alphaEq' _ _ _ _ = return False

appEq :: Subst -> Subst -> Tm -> Tm -> Tm -> Tm -> EqM Bool
appEq s1 s2 t1a t1b t2a t2b = do
  b1 <- alphaEq' s1 s2 t1a t2a
  b2 <- alphaEq' s1 s2 t1b t2b
  return $ b1 && b2
  

-- | Substitute a with b.
(-->) :: Var -> Var -> (Var -> Var)
(-->) a b c | c == a    = b
            | otherwise = c
