-------------------------------------------------------------------------------
-- |
-- Module      :  CCO.HM.Parser
-- Copyright   :  (c) 2008 Utrecht University
-- License     :  All rights reserved
--
-- Maintainer  :  stefan@cs.uu.nl
-- Stability   :  provisional
-- Portability :  portable
--
-- A 'Parser' for a simple, implicitly typed functional language.
--
-------------------------------------------------------------------------------

module CCO.HM.Parser (
    -- * Parser
    parseFile    -- :: Component String Tm
) where

import CCO.HM.Base                     (Var, Tm (Tm), Tm_ (Var, Lam, App, Bottom, Unit, StrictApp))
import CCO.HM.Lexer                    (Token, lexer, keyword, var, spec)
import CCO.Component                   (Component)
import qualified CCO.Component as C    (parser)
import CCO.Parsing
import Control.Applicative
import CCO.SourcePos                   (Source (..))
import CCO.Feedback                    (Feedback, runFeedback)
import System.IO
import System.Exit                     (exitFailure)

-------------------------------------------------------------------------------
-- Token parsers
-------------------------------------------------------------------------------

-- | Type of 'Parser's that consume symbols described by 'Token's.
type TokenParser = Parser Token

-------------------------------------------------------------------------------
-- Parser
-------------------------------------------------------------------------------


parseFile :: String -> IO Tm
parseFile f = do
  input <- readFile f
  result <- runFeedback (parse_ lexer (pTm <* eof) (File f) input) 1 1 stderr
  case result of
    Nothing     -> exitFailure
    Just out    -> return out

-- | Parses a 'Tm'.
pTm :: TokenParser Tm
pTm = choice [pLambda, pApp, pSApp]

pVar = (\pos x -> Tm pos (Var x))  <$> sourcePos <*> var
pUnit = (\pos _ _ -> Tm pos Unit) <$> sourcePos <*> spec "(" <*> spec ")"
pBottom = (\pos _ -> Tm pos Bottom) <$> sourcePos <*> spec "~"
pLet = (\pos x t1 t2 -> Tm pos (App (Tm pos (Lam x t2)) t1)) <$>
        sourcePos <* keyword "let" <*> var <* spec "=" <*> pTm <*
        keyword "in" <*> pTm <* keyword "ni"
pTmParens = spec "(" *> pTm <* spec ")"
pLambda = (\pos x t1 -> Tm pos (Lam x t1)) <$>
          sourcePos <* spec "\\" <*> var <* spec "." <*> pTm

pApp = (\pos ts -> foldl1 (\t1 t2 -> Tm pos (App t1 t2)) ts) <$>
        sourcePos <*> some ctxTerms

pSApp = (\pos ts -> foldl1 (\t1 t2 -> Tm pos (StrictApp t1 t2)) ts) <$>
        sourcePos <*> someSepBy (spec "$!") ctxTerms

ctxTerms = choice [pVar, pUnit, pBottom, pLet, pTmParens]
