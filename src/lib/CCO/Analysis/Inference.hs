{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, MultiParamTypeClasses, FunctionalDependencies #-}

-- | This module contains functions for performing type inference

module CCO.Analysis.Inference where

import CCO.Analysis.Type
import CCO.Analysis.AnnType
import CCO.HM.Base (Tm, Tm_, Var)
import Data.Set hiding (map)

-- | Represents a type substitution
type TySubst ty = (TySubst' ty) -> (TySubst' ty)

-- | Internal type substitution representation
data TySubst' ty
  = (:~>) TyVar ty (TySubst' ty)
  | AnnSubst AnnVar AnnVar (TySubst' ty)
  | Id

ftv :: AnnTy -> Set TyVar
ftv (ATyVar v) = singleton v
ftv ((:-->) _ t1 _ t2 _) = ftv t1 `union` ftv t2
ftv ATyUnit = empty

(~~) :: TySubst AnnTy -> AnnTy -> AnnTy
sub ~~ ty = apply (sub Id) ty
  where apply Id ty          = ty
        apply ((:~>) a b n) ty = apply' a b (apply n ty)
        apply (AnnSubst a b n) ty = apply2 a b (apply n ty)

        apply' :: TyVar -> AnnTy -> AnnTy -> AnnTy
        apply' a b (ATyVar v) | v == a = b
        apply' a b t@(ATyVar _)          = t
        apply' a b ((:-->) psi1 t1 phi t2 psi2) = (:-->) psi1 (apply' a b t1) phi (apply' a b t2) psi2
        apply' _ _ t                    = t

        apply2 :: AnnVar -> AnnVar -> AnnTy -> AnnTy
        apply2 a b ((:-->) psi1 t1 phi t2 psi2) = (:-->) psi1' t1' phi' t2' psi2'
          where t1' = apply2 a b t1
                t2' = apply2 a b t2
                psi1' = change psi1
                phi' = change phi
                psi2' = change psi2
                change l | a == l = b
                change l = l
        apply2 _ _ t = t

unify :: AnnTy -> AnnTy -> TySubst AnnTy
unify (ATyVar t1) (ATyVar t2) | t1 == t2 = id
unify a1@(ATyVar v) t2 | v `member` (ftv t2) = error (occursCheck a1 t2)
unify (ATyVar v) t2 = v :~> t2
unify t1 a2@(ATyVar v) | v `member` (ftv t1) = error (occursCheck a2 t1)
unify t1 (ATyVar v) = v :~> t1
unify ((:-->) psi11 t11 phi1 t12 psi12) ((:-->) psi21 t21 phi2 t22 psi22) = s2 . s1
  where s0 = AnnSubst psi11 psi21 . AnnSubst phi1 phi2 . AnnSubst psi12 psi22
        s1 = unify (s0 ~~ t11) (s0 ~~ t21)
        s2 = unify (s1 ~~ t12) (s1 ~~ t22)
unify t1 t2 = error (notUnifiable t1 t2)


type Env k ty = [(k, ty)]

-- | Lift pointwise
ftvEnv :: Ord k => Env k AnnTy -> Set TyVar
ftvEnv env = unions $ map (ftv . snd) env

applySub :: TySubst AnnTy -> Env k AnnTy -> Env k AnnTy
applySub s = map (\(x,t) -> (x, s ~~ t))
 

--------------------------------------------------------------------------------
-- Error messages
--------------------------------------------------------------------------------

occursCheck :: (Show ty) => ty -> ty -> String
occursCheck t1 t2 = "Occurs check: Cannot construct the infinite type " ++ ty
  where ty = unwords [show t1, "=", show t2]

notUnifiable :: (Show ty) => ty -> ty -> String
notUnifiable t1 t2 = "Could not match type " ++ tys
  where tys = unwords [show t1, "with", show t2]

-- TODO : move to PrettyPrinting
instance (Show ty) => Show (TySubst ty) where
  show s = show (s Id)

instance (Show ty) => Show (TySubst' ty) where
  show ((:~>) a b n) = "[(" ++ (show a) ++ ") --> (" ++ (show b) ++ ")]" ++ show n
  show (AnnSubst a1 a2 n) = "[(" ++ (show a1) ++ ") ~~> (" ++ (show a2) ++ ")]" ++ show n
  show Id = ""
