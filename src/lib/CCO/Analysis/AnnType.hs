module CCO.Analysis.AnnType where

import CCO.Analysis.Type

type AnnVar = String
type PhiVar = String
type PsiVar = String

-- | The types available in the language
data AnnTy
  = ATyVar TyVar -- Ann
  | (:-->) PsiVar AnnTy PhiVar AnnTy PsiVar -- It looks like you can't use it as infix
  | ATyUnit -- Ann
  deriving (Show, Eq, Ord)

-- Annotated type + demanded neediness and applicativeness
data ATy = ATy AnnTy PhiVar PsiVar

-- | Represents an environment containing type environment
type ATyEnv = [(Var, ATy)]

toTy :: AnnTy -> Ty
toTy (ATyVar v) = TyVar v
toTy ATyUnit = TyUnit
toTy ((:-->) psi1 ty1 phi ty2 psi2) = (toTy ty1) :-> (toTy ty2)
