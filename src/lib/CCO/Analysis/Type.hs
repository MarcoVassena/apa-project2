{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

module CCO.Analysis.Type where

type Var   = String    -- ^ Type of variables.
type TyVar = String

-- | The types available in the language
data Ty
  = TyVar   TyVar
  | (:->) Ty Ty
  | TyUnit
  deriving (Show, Eq, Ord)

-- | Represents a type environment
type TyEnv = [(Var, Ty)]
