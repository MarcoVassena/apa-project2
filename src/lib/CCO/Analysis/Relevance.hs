-- | This module defines the lattice used for strictness analysis.

module CCO.Analysis.Relevance where

import CCO.Analysis.Lattice

-- | Relevance implice strictness.
data Relevance = S   -- ^ Relevant abstraction
               | L   -- ^ Could be relevant or not.
  deriving (Show , Eq)

instance Lattice Relevance where
  join S x = x
  join L _ = L

  meet S _ = S
  meet L x = x

  top = L
  bottom = S

  L <: S = False 
  _ <: _ = True
