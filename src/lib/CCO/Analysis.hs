-- | Perform strictness analyisis on a minimal simply typed lambda calculus
-- enriched with strictness application

module CCO.Analysis (constraintsOf, annTypeOf) where

import CCO.Analysis.AG (constraintsOf, annTypeOf)
