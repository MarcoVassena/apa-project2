module Main where

import CCO.HM           (parseFile)
import CCO.Analysis
import CCO.Constraints
import CCO.PPrint
import Options.Applicative
import Data.Map as M

data Options = Options
  { file :: String
  }

options :: Parser Options
options = Options
  <$> argument str (metavar "<FILE>")
  
pOpts :: ParserInfo Options
pOpts = info (helper <*> options)
  (fullDesc <> progDesc "Strictness analysis")

main :: IO ()
main = do
  opts <- execParser pOpts
  ast <- parseFile (file opts)
  
  putStrLn "Input:"
  putStrLn $ pprint ast
  putStrLn "\n\n"

  let cs = constraintsOf ast
  putStrLn $ "Constraints: "
  mapM_ (putStrLn . pprint) cs
  putStrLn "\n\n"

  let annTy = annTypeOf ast
  putStrLn $ "Annotated type: " ++ pprint annTy

  let sol = solve cs
  putStrLn $ "Solution: (CORRECT NOW?) "
  mapM_ (putStrLn . pprint) (M.toList sol)
  putStrLn "\n\n"


  return ()
