\documentclass[12pt, a4paper, oneside]{article}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{placeins}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{textcomp}
\usepackage{plstx}
\usepackage[toc,page]{appendix}
\usepackage[T1]{fontenc}
\usepackage{bussproofs}

% font size could be 10pt (default), 11pt or 12 pt
% paper size coulde be letterpaper (default), legalpaper, executivepaper,
% a4paper, a5paper or b5paper
% side coulde be oneside (default) or twoside 
% columns coulde be onecolumn (default) or twocolumn
% graphics coulde be final (default) or draft 
%
% titlepage coulde be notitlepage (default) or titlepage which 
% makes an extra page for title 
% 
% paper alignment coulde be portrait (default) or landscape 
%
% equations coulde be 
%   default number of the equation on the rigth and equation centered 
%   leqno number on the left and equation centered 
%   fleqn number on the rigth and  equation on the left side
%   

\newcommand\doubleplus{+\kern-1.3ex+\kern0.8ex}
\newcommand\mdoubleplus{\ensuremath{\mathbin{+\mkern-10mu+}}}


\title{APA Project 2 - Being lazy in being lazy}
\author{Marco Vassena  \\
    4110161 \\
    \and
    Philipp Hausmann \\
    4003373 \\
    }

\date{\today}
\begin{document}



\maketitle

\tableofcontents

\section{Introduction}
This document contains the documentation of the second APA project. Please see the file README.txt for build instructions.
Our project is structured as follows.

\paragraph{\texttt{CCO.HM}} Ast definition, lexer and parser for the language under analysis.
\paragraph{\texttt{CCO.Analysis}} Analysis related modules.
\paragraph{\texttt{CCO.Analysis.Inference}} Unification and type substitution for annotated types.
\paragraph{\texttt{CCO.Analysis.AnnType}} Definition of the annotated types.
\paragraph{\texttt{CCO.Analysis.AG.Strictness}} Performs the strictness analysis.
\paragraph{\texttt{CCO.Analysis.AG.Transform}} Based on the annotated types performs the sought optimization.
\paragraph{\texttt{CCO.Analysis.AG.Debug}} Provides useful debugging information about the analysis of the input program. Includes the rules stack trace and logging of the type substitution.
\paragraph{\texttt{CCO.PPrint}} Pretty printing functions for various objects involved in the analysis.

\section{Strictness Analysis}
\label{sec:strictness}
Our project is an implementation of the strictness analysis described in \cite{HH10}. Strictness analysis consists of finding out which function applications in a program are strict in their argument. If this information is known, it is possible to optimize a program by transforming all such applications to strict applications. Although this analysis is straightforward for the nonstrict $\lambda$-calculus, it is definitively more involved if the language is enriched with an explicit strict application construct.

\subsection{Language}
The language under analysis is the minimal language described in the paper. The language contains lambdas, application, strict application, variables, bottom and unit.
In our implementation the symbol $\sim$ is used for $\bot$.
Our language supports also let bindings, which are desugared to lambas.	 
The language is monomorphic.

\subsection{Analysis}
Our analysis is monovariant and based on the syntax rules described in \cite{HH10}.  We decided to use the \texttt{UUAGC } system becuse these rules can be implemented directly as an attribute grammar.
More precisely the elements before $\rhd$ are inherited attributes and those after are synthesized.
Therefore the attribute grammar \texttt{Strictness.ag} defines the following attributes:
\begin{itemize}
	\item \texttt{annEnv}  : $\widehat{\Gamma}$
	\item \texttt{phi} : $\phi$
	\item \texttt{psi} : $\psi$
	\item \texttt{annTy} : $\widehat{\tau}$
\end{itemize}
Where \texttt{annEnv} is a chained through attribute, \texttt{phi} and \texttt{psi} are inherited and \texttt{annTy} is synthesized.
In addition to those there is the synthesized attribute \texttt{sub}, which represents an annotated type substitution, that addresses both principal types and annotations


The non-standard type system implemented extends the types with the annotations $\phi$ and $\psi$ and piggy-back on algorithm W, which has been extended to deal with them in two different approaches described in \ref{sec:approaches}.

\section{Approaches. Better name?}
\label{sec:approaches}
We have implemented the analysis with two different approaches, the first is unification-based and the second is constraint-based.
These are described in detail in the following and finally compared in section
\ref{section:comparison}.

\subsection{Unification}
With this method, annotations are unified on the spot while typing the input program. The current implementation of the analysis is not conservative, meaning that well-typed programs can be rejected if the unification of two annotations fails,
which happens for instance if they have been assigned opposite values (S and L).

\paragraph{Motivation}
We decided at first to follow this approach instead of a constraint-based approach, because it was not clear what kind of constraints should be generated to implement the analysis correctly.
Furthermore, the annotations variables $\phi$ and $\psi$ are combined with both $\sqcup$ and $\sqcap$. This requires an advanced constraints language which includes the $\sqcup$ and $\sqcap$ operators. It was not entirely clear what algorithm would be suitable to
solve such constraints systems, and if the generated constraints would always be solvable.
In addition also the prototype implementation of the analysis described in \cite{HH10} opts for unification.

Lastly, another reason that convinced us to pursue this approach is that the syntax directed rule \emph{s-abs} defined in \cite{HH10} uses a selecting function $sel$ that actually inspects the value of the demanded applicativeness $\psi$ to choose the updated (synthesized) environment $\widehat{\Gamma}$ to return.
This suggests that the annotations variables should always be assigned a value during the analysis, which can be done unifying annotation variables on the fly.

With this approach subeffecting is achieved through the updated environment $\Gamma'$, and this we were surprised to find out that the analysis is not conservative. We cannot tell exactly whether this is a bug in our implementation or the implementation is supposed to handle particularly these cases.	

\paragraph{Implementation}
At first glance it could seem that once correctly initialized the demanded relevance $\phi$ and $\psi$ all the remaining annotations could be directly computed using the updated environment.
However this is not the case because in the \emph{s-abs} rule $\phi_2$ is a fresh variable that remains free until unification occurs in either \emph{s-lapp} or \emph{s-eapp} rule.
Another implementation detail that is somehow ignored by those rules regards the $sel$ function. This function returns the original environment $\widehat{\Gamma}$ or the updated one $\widehat{\Gamma'}$ depending on the value of the demanded applicativeness $\psi$. However, due to the definition of \emph{s-abs} such $\psi$ could be a variable not yet bound.
A possible workaround in such case would be to default the choice to original environment $\widehat{\Gamma}$. This would be sound but so imprecise that the whole analysis would become almost useless. In fact the situation in which $\psi$ is not bound is very common and happens precisely every time there are at least two consecutive lambda abstraction. Therefore defaulting would prevent programs as simple as \texttt{$(\lambda x . \lambda y . x) () \bot$} from being optimized.
The problem basically consists in the fact that the \emph{s-abs} rule depends on information present in the \emph{s-lapp} and \emph{s-eapp} rules.
Namely the demanded fresh variable $\psi_2$ in the \emph{s-abs} will assume the  value of the demanded $\psi$ in an the application rules. This information is not available until unification, which occurs at a later time, nevertheless the $sel$ function already needs it.
In order to propagate this information correctly an additional inherited attribute \texttt{psiStack} ($\Psi$) is defined.
In the application rules the same value passed as demanded applicativeness to each child is pushed on their stack.
In the lambda rules if the demanded applicativeness $\phi$ is not bound yet  the value on top of the stack is used instead: it will be the value to which the variable will be bound. When this happens that value is also popped from the stack before threading it to the body of the function.
The following modifications of the original rules describe precisely this behaviour. The rules not mentioned simply ignore this attribute.
This rule applies when $\psi$ is bound.
\begin{prooftree}
\AxiomC{$\widehat{\Gamma} \mdoubleplus [x \rightarrow \tau^{\left(\phi_0, \psi_0\right)}] ; \Psi; S ; \psi_2	 \vdash e_1 \rhd e_1' :: \widehat{\tau_2} ; \widehat{\Gamma'} \mdoubleplus [x \rightarrow \tau_1^{\left( \phi_1, \psi_1 \right)}] $}
\UnaryInfC{$ \widehat{\Gamma'} ; \Psi; \phi ; \psi  \vdash \lambda x \rightarrow e_1  \rhd \lambda x \rightarrow e_1' :: \widehat{\tau_1}^{\psi_1} \xrightarrow{\phi_1} \widehat{\tau_2}^{\psi_2} ; sel(\psi, \widehat{\Gamma}, \widehat{\Gamma'})$}
\end{prooftree}
This rule instead applies when $\psi$ is not bound yet.
\begin{prooftree}
\AxiomC{$\widehat{\Gamma} \mdoubleplus [x \rightarrow \tau^{\left(\phi_0, \psi_0\right)}] ; \Psi; S ; \psi_2	 \vdash e_1 \vdash e_1' :: \widehat{\tau_2} ; \widehat{\Gamma'} \mdoubleplus [x \rightarrow \tau_1^{\left( \phi_1, \psi_1 \right)}] $}
\UnaryInfC{$ \widehat{\Gamma} ; (\psi' : \Psi); \phi ; \psi  \vdash \lambda x \rightarrow e_1  \rhd \lambda x \rightarrow e_1' :: \widehat{\tau_1}^{\psi_1} \xrightarrow{\phi_1} \widehat{\tau_2}^{\psi_2} ; sel(\psi', \widehat{\Gamma}, \widehat{\Gamma'})$}
\end{prooftree}
In case $\psi'$ is also not bound or the stack is empty the original environment $\Gamma$ is returned, as this is always sound.
For the application rules:
\begin{prooftree}
\alwaysNoLine
\AxiomC{$\widehat{\Gamma} ; (\phi : \Psi) ; \phi ; \phi \vdash e_1 \rhd e_1' :: \widehat{\tau_2}^{\psi_2} \xrightarrow{\phi_0} \widehat{\tau}^{\psi} ; \widehat{\Gamma''}$}
\UnaryInfC{$\widehat{\Gamma''} ; (\phi \sqcup \psi_2  : \Psi) ; \phi \sqcup \phi_0; \phi \sqcup \psi_2 \vdash e_1 \rhd e_1' :: \widehat{\tau_2}		 ; \widehat{\Gamma'}$}
\alwaysSingleLine
\UnaryInfC{$ \widehat{\Gamma} ; \Psi; \phi ; \psi  \vdash e_1\  e_2 \rhd app (\phi_0, e_1', e_2') :: \widehat{\tau} ; \Gamma'$}
\end{prooftree}

\begin{prooftree}
\alwaysNoLine
\AxiomC{$\widehat{\Gamma} ; (\phi : \Psi) ; \phi ; \phi \vdash e_1 \rhd e_1' :: \widehat{\tau_2}^{\psi_2} \xrightarrow{\phi_0} \widehat{\tau}^{\psi} ; \widehat{\Gamma''}$}
\UnaryInfC{$\widehat{\Gamma''} ; (\phi \sqcup \psi_2  : \Psi) ; \phi ; \phi \sqcup \psi_2 \vdash e_1 \rhd e_1' :: \widehat{\tau_2}		 ; \widehat{\Gamma'}$}
\alwaysSingleLine
\UnaryInfC{$ \widehat{\Gamma} ; \Psi; \phi ; \psi  \vdash e_1\ \$!\ e_2 \rhd e_1'\ \$!\ e_2'  :: \widehat{\tau} ; \Gamma'$}
\end{prooftree}

\subsection{Constraint-based approach}
In parallel to the unification approach we also implemented a solution based on constraints. To be more precise, the constraints are only used for the annotations, the underlying type system still uses direct unification.
As the paper \cite{HH10} does not use constraints, it is not immediately obvious how the constraints should be generated and of what form they should be.

\paragraph{Constraint Language}
The constraints language has to be sufficiently rich to encode all the required constraints. The constraints look as follows:
\begin{plstx}
*(annotation values):     v [\in] \{S, L\} \\
*(annotation variables):  x [\in] \mathit{AnnVar} \\
(constraints):  C ::=   t == x | t <= x \\
(terms):        t ::=   f | f_1 \sqcup f_2 | f_1 \sqcap f_2 | if \; b \; then \; f_1 \; else \; f_2 \\
(values or variables):         f  ::=   v | x \\
(boolean expressions):  b   ::= t_1 == t_2 \\
\end{plstx}
The chosen design allows
delaying make any final choice for annotation variables until all constraints are collected. 
This makes a clear separation between the constraint collecting phase and the solving phase possible.

\paragraph{Generating Constraints}
We will discuss the generated constraints per language construct. The invariant holding for all constructs is, that the
annotations inside the types and the environment are always annotation variables, but never values or other terms. The constraints
themselves can be collected using a synthesized attribute, as no intermediate solving takes place and the ordering
is irrelevant.


As a base case, the unit and bottom constructs never cause any constraints to be generated.
For the other cases, fresh annotation variables are inserted in all places where two or more annotations are combined (eg. $\sqcap$ or $\sqcup$). This new variable is then constraint to be equal to applying the combination operator to the original variables.

For the environment update in the variable rule, the selection process is moved inside the environment using if-then-else constraints.
This is possible because the updated and old environment are structurally equivalent.

As a special case, the constraint for application for the $\psi$ variable uses under or equal to enable sub effecting.

\paragraph{Constraint Solving}
The constraints are solved in a two step process. First, if two variables are constrained to be equal, one of the variables
is dropped and all it's occurences are replaced by the other one. This is repeated until there are no more such variables.

This simplified constraint system is then solved using fix-point iteration with the work list algorithm. Because the S/L-lattice used
has ACC, the fix-point iteration will always terminate in finite time. But because not all constraint systems are solvable,
the solution needs to be verified. The verification is straight forward, as the fix-point iteration will assign a value
to each variable.

\paragraph{Transformation}
If a valid solution exists, the program is transformed. To decide where to optimize the values for the annotation variables
are looked up in the solution.

If no valid solution has been found, the program aborts and reports an error. Hence it is not conservative, but it would be easily possible
to instead return the original input as result.


\section{Comparison}
\label{section:comparison}

In this section we will compare the results of both approaches against each other. For this we will use some of the examples from the
paper \cite{HH10} as well as some other examples.

\paragraph{Example 1}
As a first example, we take the first example from section 4.3 of the paper (tests/paper-4.3-a.inp.lam).
\begin{lstlisting}
(\x . (\y . x)) () ()
\end{lstlisting}
Both the unify and constraint approach transform this into the following sound and optimal program:
\begin{lstlisting}
((\x . (\y . x)) $! ()) ()
\end{lstlisting}

\paragraph{Example 2}
The second example is example 3 from the paper, section 5.2 (tests/paper-5.2-3.inp.lam):
\begin{lstlisting}
(\x . ((\y . ()) $! (\z . x))) ~
\end{lstlisting}
Both approaches do not apply any transformation, as converting the lazy application to strict
would cause the transformed program to diverge:
\begin{lstlisting}
(\x . ((\y . ()) $! (\z . x))) ~
\end{lstlisting}

\paragraph{Example 3}
This example shows that for some cases, the results of the approaches differs. The example is as follows (tests/applyId1.inp.lam):
\begin{lstlisting}
(\f . (\x . f x)) (\a . a) ()
\end{lstlisting}
The unify-approach will not transform the "f x" application:
\begin{lstlisting}
((\f . (\x . f x)) $! (\a . a)) $! ()
\end{lstlisting}
The constraint-approach on the other hand does, as it delays the decision long enough to see that this is a valid
transformation for all possible values of f.
\begin{lstlisting}
((\f . (\x . f $! x)) $! (\a . a)) $! ()
\end{lstlisting}


\paragraph{Example 4}
This last example shows one case where both approaches fail (let2-failing.lam):
\begin{lstlisting}
let id = \x . x in let f = \x . () in (id f) ((id (\x . x)) ~) ni ni
\end{lstlisting}
This is because the id function is applied twice to functions with differing strictness annotations, which
causes unification/constraint solving failures. This problem could be solved by changing the subeffecting rules, but it is
not immediately obvious from the paper what would need to be changed while still preserving soundness. Another option
would be to make the analysis polyvariant, which should solve the problem as well.



\section{Conclusion}
We have shown two approaches to implementing a non-polyvariant strictness analysis. Our implementations are sound and for the examples
in the paper on which this work is based optimal as well. For other examples this does not hold and the analysis might even fail, it is not
conservative.

To solve some of the remaining issues and improve the precision, the subeffecting rules could be adapted and the analysis could be
made polyvariant. As the paper does not explicitly describe the algorithm, but mostly just the formal deduction rules it is not
clear what changes in detail this would entail. This has also been a challenge we faced during implementing the algorithm
described in the paper, as it is rather vague in where the subeffecting takes place in the syntax directed algorithm.

While we could not implement all the features we planned (polyvariance), we got two working solution for the non-polyvariant analysis.
Comparing the two solutions, it appears that the constraint-based approach yields slightly better results while from a 
code-design point of view both are viable approaches.

\begin{thebibliography}{1}

\bibitem{HH10}
  Holdermans, S. and Hage, J.
  \emph{Making "Stricterness" More Relevant (TODO Add year)}


\end{thebibliography}



\end{document}
